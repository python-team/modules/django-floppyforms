Source: django-floppyforms
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Michael Fladischer <fladi@debian.org>
Build-Depends: debhelper-compat (= 9),
               dh-python,
               libjs-openlayers,
               python-all,
               python-coverage,
               python-django,
               python-django (>= 1.6) | python-django-discover-runner,
               python-gdal,
               python-pil,
               python-setuptools,
               python3-all,
               python3-coverage,
               python3-django,
               python3-gdal,
               python3-pil,
               python3-setuptools,
               python-sphinx
Standards-Version: 3.9.8
Homepage: https://github.com/brutasse/django-floppyforms
Vcs-Git: https://salsa.debian.org/python-team/packages/django-floppyforms.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/django-floppyforms

Package: python-django-floppyforms
Architecture: all
Depends: python-django,
         libjs-openlayers,
         ${misc:Depends},
         ${python:Depends}
Suggests: python-django-floppyforms-doc
Description: better control of form rendering in Django
 Django-foppyforms is an application that gives you full control of the
 output of forms rendering. This is more a widgets library than a forms
 library but form fields are provided for convenience. The forms API and
 features are exactly the same as Django’s, the key difference is that
 widgets are rendered in templates instead of using string interpolation.
 .
 The widgets API allows you to customize and extend the widgets behaviour,
 making it very easy to define custom widgets. The default widgets are very
 similar to the default Django widgets, except that they implement some
 nice features of HTML5 forms, such as the placeholder and required
 attribute, as well as the new <input> types.

Package: python3-django-floppyforms
Architecture: all
Depends: python3-django,
         libjs-openlayers,
         ${misc:Depends},
         ${python3:Depends}
Suggests: python-django-floppyforms-doc
Description: better control of form rendering in Django (Python3 version)
 Django-foppyforms is an application that gives you full control of the
 output of forms rendering. This is more a widgets library than a forms
 library but form fields are provided for convenience. The forms API and
 features are exactly the same as Django’s, the key difference is that
 widgets are rendered in templates instead of using string interpolation.
 .
 The widgets API allows you to customize and extend the widgets behaviour,
 making it very easy to define custom widgets. The default widgets are very
 similar to the default Django widgets, except that they implement some
 nice features of HTML5 forms, such as the placeholder and required
 attribute, as well as the new <input> types.
 .
 This package contains the Python 3 version of the library.

Package: python-django-floppyforms-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: better control of form rendering in Django (Documentation)
 Django-foppyforms is an application that gives you full control of the
 output of forms rendering. This is more a widgets library than a forms
 library but form fields are provided for convenience. The forms API and
 features are exactly the same as Django’s, the key difference is that
 widgets are rendered in templates instead of using string interpolation.
 .
 The widgets API allows you to customize and extend the widgets behaviour,
 making it very easy to define custom widgets. The default widgets are very
 similar to the default Django widgets, except that they implement some
 nice features of HTML5 forms, such as the placeholder and required
 attribute, as well as the new <input> types.
 .
 This package contains the documentation.
